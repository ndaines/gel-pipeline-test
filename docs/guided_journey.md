# Global Experience Library (Docs)
## Guided Journey

### Description


### JavaScript
class: `GuidedJourney`
```
GuidedJourney
  |- constructor
  |  Initialise the Guided Journey component
  | 
  |- handleHashChange
  |  Function that is called whenever the page hash changes
  |  
  |- getLocalContent
  |  Is called when there is JSON injected into the page
  |
  |- getRemoteContent
  |  Is called when a URL is supplied to fetch content
```

// Preferance local (dom)
// Then local (json)
// Then Network


constructor:
1. Store the initial state in memory (possibly use: gel-guided-journey__content) <br/>
    Or could have (gel-guided-journey__landing)
    
    
2. Get location (href), and render content




Functions:
- Navigate using the LHS nav
- Back a page
- Next page