(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "core/notificationCentre", "utilities/cookieControl", "jquery"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("core/notificationCentre"), require("utilities/cookieControl"), require("jquery"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.notificationCentre, global.cookieControl, global.jquery);
    global.user = mod.exports;
  }
})(this, function (_exports, _notificationCentre, _cookieControl, $) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports["default"] = void 0;
  _notificationCentre = _interopRequireDefault(_notificationCentre);
  _cookieControl = _interopRequireDefault(_cookieControl);
  $ = _interopRequireWildcard($);

  function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj["default"] = obj; return newObj; } }

  function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  /**
   * The User module exposes user details stored in cookies (set when a user logs in). While this class can be instantiated as separate  instance objects it is really designed to be used in GEF as a global singleton
   *
   * @since 1.7.0
   *
   * @author Alex Motyka <alex.motyka@det.nsw.edu.au>, 
   * @copyright © 2019 State Government of NSW 2016
   *
   * @class
   */
  var User =
  /*#__PURE__*/
  function () {
    /**
     * Constructor for User
     *
     * @constructor
     *
     * @param {String|Element|jQuery} [selector] - Either a CSS selector, DOM element or matched jQuery object
     * @param {Object} config - class configuration options
     *
     * @example
     * // Instantiate a new DiagnosticHUD
     * let diagnosticHUD = new DiagnosticHUD(".gef-global-header", {})
     */
    function User(selector, config) {
      _classCallCheck(this, User);

      // Check if only one argument has been passed
      if (arguments.length === 1) {
        // If argument[0] is a config object then set the config arg and nullify the selector arg
        if (selector instanceof Object && !(selector instanceof $)) {
          config = selector;
          selector = null;
        }
      } // Deffault config options


      this.config = {
        selectors: {
          pageTitle: "title",
          divisionTitle: ".gef-section-header__logo a"
        },
        cookies: {
          userID: "UserID",
          userBrowsingHistory: "UserBrowsingHistory",
          userDisplayName: "UserDisplayName",
          userRoles: "UserRoles"
        } // Check if config has been passed to constructor

      };

      if (config) {
        // Merge default config with passed config
        this.config = $.extend(true, {}, this.config, config);
      }

      if (selector) {
        this.$container = $(selector);
      } // Set up a instance level reference to the NotificationCentre singleton


      this.notificationCentre = _notificationCentre["default"].shared();
      this.cookieController = new _cookieControl["default"](); // Get the user's browsing history from the UserBrowsingHistory cookie

      var browsingHistoryCookie = this.cookieController.returnCookie(this.config.cookies.userBrowsingHistory);
      var browsingHistoryArray = JSON.parse(decodeURIComponent(browsingHistoryCookie)); // Add the user's browsing history stored in the UserBrowsingHistorycook to instance history property

      if (browsingHistoryArray) {
        this.history = browsingHistoryArray;
      } else {
        this.history = [];
      } // Get the current current 


      var pageTitle = $(this.config.selectors.pageTitle).text().split("|")[0].trim();
      var pageURL = document.location.href; // Grab the division name and URL from the page header

      var $divisionNameAnchor = $(this.config.selectors.divisionTitle);

      if ($divisionNameAnchor.length > 0) {
        // Add the page to the history array
        this.addPage(pageTitle, pageURL, $divisionNameAnchor.first().text(), $divisionNameAnchor.first().attr("href"));
      } // Hashed user's DETUserGUID


      var idCookie = document.cookie.match(new RegExp("".concat(this.config.cookies.userID, "=([^;]+)")));

      if (idCookie) {
        this.id = idCookie[1];
      } // User's displayname e.g. "John Doe"


      var displayName = document.cookie.match(new RegExp("".concat(this.config.cookies.userDisplayName, "=([^;]+)")));

      if (displayName) {
        this.displayName = displayName[1];
      } // User's Active Directory detAttribute15 (i.e. their role, not to be confused with title). Can have multiple.


      var rolesCookie = document.cookie.match(new RegExp("".concat(this.config.cookies.userRoles, "=([^;]+)")));

      if (rolesCookie) {
        this.roles = decodeURI(rolesCookie[1]).split("|");
      } // Diagnostic update


      this.notificationCentre.publish("diagnosticUpdate", {
        module: "User",
        messages: [{
          text: this.config.cookies.userID,
          variable: this.id
        }, {
          text: this.config.cookies.userRoles,
          variable: this.roles
        }, {
          text: this.config.cookies.userDisplayName,
          variable: this.displayName
        }]
      });
    }
    /**
     * Add a page to the user's history
     * 
     * @param {String} pageTitle - Page title
     * @param {String} pageUrl - Page URL
     * @param {String} divisionName - Division name
     * @param {String} divisionUrl - Division URL
     */


    _createClass(User, [{
      key: "addPage",
      value: function addPage(pageTitle, pageUrl, divisionName, divisionUrl) {
        var historyItem = {
          title: pageTitle,
          url: pageUrl,
          division: {
            title: divisionName,
            url: divisionUrl
          },
          created: new Date().toISOString() // Add the new history item to the begining of the array

        };
        this.history.unshift(historyItem); // If the array has more than 10 items drop the last item

        if (this.history.length > 10) {
          this.history.pop();
        } // Update the UserBrowsingHistory cookie with new array


        this.cookieController.setCookie(this.config.cookies.userBrowsingHistory, encodeURIComponent(JSON.stringify(this.history)), 31536000);
        return this.history;
      }
      /**
       * Static function to instantiate the User class as singleton
       *
       * @static
       *
       * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
       * @param {object} config   - class configuration arguments. Refer to class constructor for complete documentation of the config object
       *
       * @returns {User} Reference to the same User instantiated in memory
       */

    }], [{
      key: "shared",
      value: function shared(selector, config) {
        return this.instance != null ? this.instance : this.instance = new User(selector, config);
      }
    }]);

    return User;
  }();
  /**
   * Exports the tabpanelPersonalisation class as a JS module
   * @module
   */


  var _default = User;
  _exports["default"] = _default;
});