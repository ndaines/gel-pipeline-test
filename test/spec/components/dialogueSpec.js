// "use strict"

// var dialogueSpec = function(Dialogue) {

//   describe("Dialogue has is initialised", function() {
//     var component,
//         $object,
//         selector = "[data-gef-dialogue-trigger]"

//     beforeEach(function() {
//       loadFixtures("components/dialogue.html")
//       $object = $j(selector)
//       component = new Dialogue(selector)
//     })

//     it("Sets the $object to be the component's trigger", function() {
//       console.log("COMPONENT" + component)
//       expect($object[0]).toBe(component.config.$trigger)
//     })

//     it("Matched content element", function() {
//       expect(component.contents.length).toBe(1)
//     })

//     it("Stored the correct content into component.contents.content", function() {
//       expect(component.contents.content).toBe(component.contents.html())
//     })
//   })

//   describe("Dialogue trigger has been actioned", function() {
//     var component,
//         $object,
//         selector = "[data-gef-dialogue-trigger]"

//     beforeEach(function() {
//       loadFixtures("components/dialogue.html")
//       $object = $j(selector)
//       component = new Dialogue(selector)
//       component.showDialogue()
//     })

//     it("Has displayed the Dialogue box", function() {
//       expect($j('[data-gef-dialogue]').length).toBe(1)
//     })

//     it("Has applied the correct aria attributes to the hidden content", function() {
//       expect($object.attr('aria-hidden')).toBe('true')
//     })

//     it("Has got the correct content in the Dialogue box", function() {
//       expect($j('[data-gef-dialogue]')).toBe(component.contents.content)
//     })

//   })

// }


// define(["/base/dist/assets/js/components/dialogue.js"], dialogueSpec)