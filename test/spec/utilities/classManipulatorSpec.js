"use strict"

var classManipulatorSpec = function(ClassManipulator) {


  // Test non-object
  describe("ClassManipulator is instantiated with non-object", function() {
    var component,
        $object,
        notAnObject = ".not-an-object"

    it("Fails if there are bad arguments", function() {
      expect(function(){  new ClassManipulator() }).toThrowError(TypeError)
      expect(function(){  new ClassManipulator(notAnObject) }).toThrowError(TypeError)
    })
  })

  // No config passed
  describe("ClassManipulator instantiated without any configuration", function() {
    var component,
        $object,
        selector = ".add-a-class-to-me"

    beforeEach(function() {
      $object   = $j(selector)
    })

    it("Expects the event_type to fail", function() {
      expect(function() { new ClassManipulator($object) }).toThrowError(TypeError)
    })

  })

  // pass an event
  describe("ClassManipulator has been given an event", function() {
    var component,
    $object,
    selector  = ".add-a-class-to-me",
    config    = {
      event_type: "click"
    }

    beforeEach(function() {
      loadFixtures("utilities/classManipulator.html")
      $object = $j(selector)
      component = new ClassManipulator($object, config)
    })

    it("Expects to have a class added when event is triggered", function() {
      $object.trigger(config.event_type)
      expect($object).toHaveClass('active')
    })
  })

  // test the default class removal technique
  describe("ClassManipulator remove class event is fired", function() {
    var component,
    $object,
    selector  = ".add-a-class-to-me",
    config    = {
      event_type: "click"
    }

    beforeEach(function() {
      loadFixtures("utilities/classManipulator.html")
      $object = $j(selector)
      component = new ClassManipulator($object, config)
    })

    it("Expects the timer not be active", function() {
      expect(component.config.timer.on).toBe(false)
    })

    it("Expects to remove active class from the $object", function() {
      $object.trigger(config.event_type)
      expect($object).toHaveClass('active')
      $j('.container').trigger("click")
      expect($object).not.toHaveClass('active')
    })
  })

  // test remove class after some time
  describe("ClassManipulator has turned the timer on",   function() {
    var component,
    $object,
    timercb,
    selector  = ".add-a-class-to-me",
    config    = {
      event_type: "mouseout",
      click_outside: false,
      timer : {
        on : true,
        delay: 50
      }
    }

    beforeEach(function() {
      loadFixtures("utilities/classManipulator.html")
      $object = $j(selector)
      component = new ClassManipulator($object, config)
      timercb = jasmine.createSpy("timercb")
      jasmine.clock().install()
    })

    afterEach(function() {
      jasmine.clock().uninstall()
    })

    it("Expects to remove active class from the $object after some time", function() {
      $object.trigger(config.event_type)
      expect($object).toHaveClass('active')

      setTimeout(function() {
        timercb()
      }, config.timer.delay)

      jasmine.clock().tick(config.timer.delay + 1)
      expect($object).not.toHaveClass('active')
      expect($object.timer).toBe(undefined)


    })
  })


  // test adding and removing the class on a subject, but not the object
  describe("ClassManipulator has been passed a subject to manipulate", function() {
    var component,
    $object,
    $subject,
    timercb,
    config,
    selector  = ".add-a-class-to-me",
    subject   = ".container"

    beforeEach(function() {
      loadFixtures("utilities/classManipulator.html")
      $object   = $j(selector)
      $subject  = $j(subject)
      config    = {
        event_type: "click",
        subject: $subject,
        click_outside: true
      }
      component = new ClassManipulator($object, config)
    })

    it("Does not expect this.$subject to be $object", function() {
      expect($object).not.toBe(component.$subject)
    })

    it("Expects to add active class to the $subject", function() {
      $object.trigger(config.event_type)
      expect($subject).toHaveClass('active')
    })

    it("Expects the class to be removed when it has been triggered", function() {
      component.removeTheClass()
      expect($subject).not.toHaveClass('active')
    })
  })

  // test the toggle
  describe("ClassManipulator is allowed to toggle", function() {
    var component,
        $object,
        selector  = ".add-a-class-to-me",
        config    = {
          event_type: "click",
          toggle    : true,
          click_outside : false
        }

    beforeEach(function() {
      loadFixtures("utilities/classManipulator.html")
      $object = $j(selector)
      component = new ClassManipulator($object, config)
    })

    it("Expects to add and remove the active class", function() {

      $object.trigger(config.event_type)
      expect($object).toHaveClass('active')

      $object.trigger(config.event_type)
      expect($object).not.toHaveClass('active')
    })
  })

  describe("ClassManipulator removing classes on a outside click but not on ignored elements", function() {
    var component,
    $object,
    selector  = ".add-a-class-to-me",
    config    = {
      event_type: "click",
      toggle    : true,
      click_outside : true,
      click_outside_ignores: ".ignore-me-when-clicked-outside"
    }

    beforeEach(function() {
      loadFixtures("utilities/classManipulator.html")
      $object = $j(selector)
      component = new ClassManipulator($object, config)
    })

    it("Removes active class on body click", function() {

      $object.trigger(config.event_type)
      $j('body').trigger(config.event_type)
      expect($object).not.toHaveClass('active')

    })

    it("Expects to NOT remove the active class on ignore selector", function() {

      $object.trigger(config.event_type)

      $j(config.click_outside_ignores).trigger(config.event_type)
      expect($object).toHaveClass('active')

    })





  })
}

define(["/base/dist/assets/js/utilities/classManipulator.js"], classManipulatorSpec)