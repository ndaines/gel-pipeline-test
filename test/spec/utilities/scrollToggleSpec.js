"use strict"

var scrollToggleSpec = function(ScrollToggle) {

  describe("ScrollToggle is initiated with a custom config", function() {
    var utility,
        selector = "#container",
        checkpoint = "#checkpoint",
        config = {
          "checkpoint" : checkpoint,
          "subject"    : "#subject"
        }

    beforeEach(function() {
      loadFixtures("utilities/scrollToggle.html")
      utility = new ScrollToggle(selector, config)
    })

    it("expects the checkpoint to be selector", function() {
      expect(utility.config.container).toBe(selector)
    })

    it("finds the checkpoint selector", function() {
      expect(utility.$checkpoint).toBeDefined()
      expect(utility.$checkpoint[0]).toBe($j(config.checkpoint)[0])
    })

    it("finds the subject selector", function() {
      expect(utility.$subject).toBeDefined()
      expect(utility.$subject[0]).toBe($j(config.subject)[0])
    })

    it("is will have set config.scroller to selector", function() {
      expect(utility.scroller[0]).toBe($j(selector)[0])
    })

    it("Subject will have the passed attribute set to false", function() {
      expect(utility.$subject.attr(utility.config.passed_attr)).toBe("false")
    })
  })


  describe("ScrollToggle has passed its checkpoint element", function() {
    var utility,
        selector = "#container",
        config = {
          "checkpoint" : "#checkpoint",
          "subject"    : "#subject"
        }

    beforeEach(function() {
      loadFixtures("utilities/scrollToggle.html")
      spyOn(ScrollToggle.prototype, 'configureScrollTests').and.callThrough()
      spyOn(ScrollToggle.prototype, 'afterCheckpoint').and.callThrough()
      utility = new ScrollToggle(selector, config)
    })

    it("Will have called configureScrollTests", function() {
      expect(ScrollToggle.prototype.configureScrollTests).toHaveBeenCalled();
    })

    it("Will have called afterCheckpoint and set the passed_attr to true", function() {
      $j(selector).scrollTop(12000)
      $j(selector).trigger("scroll")
      expect(ScrollToggle.prototype.afterCheckpoint).toHaveBeenCalled()
      expect(utility.$subject.attr(utility.config.passed_attr)).toBe("true")
    })
  })

  describe("ScrollToggle has passed its checkpoint and then returned before the checkpoint", function() {
     var utility,
        selector = "#container",
        config = {
          "checkpoint" : "#checkpoint",
          "subject"    : "#subject"
        }

    beforeEach(function() {
      loadFixtures("utilities/scrollToggle.html")
      spyOn(ScrollToggle.prototype, 'beforeCheckpoint').and.callThrough()
      utility = new ScrollToggle(selector, config)
    })

    it("Will have set the passed_attr to true", function() {
      $j(selector).scrollTop(12000)
      $j(selector).trigger("scroll")
      expect(utility.$subject.attr(utility.config.passed_attr)).toBe("true")
    })

    it("Will have called beforeCheckpoint and set the passed_attr to false", function() {
      $j(selector).scrollTop(8000)
      $j(selector).trigger("scroll")
      expect(ScrollToggle.prototype.beforeCheckpoint).toHaveBeenCalled()
      expect(utility.$subject.attr(utility.config.passed_attr)).toBe("false")
    })
  })

  describe("ScrollToggle has a data attribute ID set to find an external subject", function() {
     var utility,
        selector = "#container2",
        subject  = "#subject2",
        config = {
          "checkpoint" : "#checkpoint2",
        }

    beforeEach(function() {
      loadFixtures("utilities/scrollToggle.html")
      utility = new ScrollToggle(selector, config)
    })

    it("Will find the external subject", function() {
      expect(utility.$subject[0]).toBe($j(subject)[0])
    })
  })

  describe("ScrollToggle has been passed a numerical checkpoint and scrolled", function() {
    var utility,
    selector = "#container2",
    config = {
      "checkpoint" : 12000
    }

    beforeEach(function() {
      loadFixtures("utilities/scrollToggle.html")
      utility = new ScrollToggle(selector, config)
    })

    it("beyond it will have set the passed_attr to true", function() {
      $j(selector).scrollTop(13000)
      $j(selector).trigger("scroll")
      expect(utility.$subject.attr(utility.config.passed_attr)).toBe("true")
    })

    it("before it will have set the passed_attr to false", function() {
      $j(selector).scrollTop(5000)
      $j(selector).trigger("scroll")
      expect(utility.$subject.attr(utility.config.passed_attr)).toBe("false")
    })
  })

  describe("ScrollToggle is scrolling the window and passed the checkpoint", function() {
    var utility,
    selector = "#container",
    config = {
      "checkpoint" : 3000,
      "scroll_window" : true,
      "subject" : "#subject"
    }

    beforeEach(function() {
      loadFixtures("utilities/scrollToggle.html")
      utility = new ScrollToggle(selector, config)
    })

    it("is will have set config.scroller to 'window", function() {
      expect(utility.scroller[0]).toBe($j(window)[0])
    })
  })

  describe("ScrollToggle has been instantiated with the config.linked setting with grouped anchors", function() {

    var utility,
    selector = ".gef-a-z-list",
    config = {
      "scroll_window" : true,
      "subject" : "[data-character-name] a",
      "linked"  : true
    }

    beforeEach(function() {
      loadFixtures("utilities/scrollToggle.html")
      spyOn(ScrollToggle.prototype, 'scrollTestMulti').and.callThrough()
      utility = new ScrollToggle(selector, config)
    })

    it("checks the subject element is an anchor tag", function() {
      expect(utility.$subject.is("a")).toBe(true)
    })

    it("finds a checkpoint(s) based on the href attribute", function() {
      expect(utility.$checkpoint.attr("id")).toBe(utility.$subject.attr("href").replace("#", ""))
    })

    it("has more than one checkpoint saved in the $checkpoint variable", function() {
      expect(utility.$checkpoint.length).toBeGreaterThan(1)
    })

    it("calls the scrollTestMulti function", function() {
      expect(ScrollToggle.prototype.scrollTestMulti).toHaveBeenCalled()
    })

    it("stores the values of the multiple checkpoints in an associative array", function() {
      expect(utility.checkpoint_arr[0].offset).toBeDefined()
      expect(utility.checkpoint_arr[4].$subject.length).toBe(1)
    })


    it('only has zero active toggles before scrolling', function() {
      // Scroll to the third element and expect 2 to no longer be highlighted
      expect($j(config.subject + "[data-gef-scrolltoggle-passed=true]").length).toBe(0)
    })

    it('only has one active toggle per subject while scrolling', function() {
      // Scroll to the third element and expect 2 to no longer be highlighted
      $j(window).scrollTop(1500)
      $j(window).trigger("scroll")
      expect($j(config.subject + "[data-gef-scrolltoggle-passed=true]").length).toBe(1)
    })

    it('first anchor has toggled when scrolled past', function() {
      $j(window).scrollTop(500)
      $j(window).trigger("scroll")
      expect(utility.$subject.eq(0).attr("data-gef-scrolltoggle-passed")).toBe("true")
    })

    it('fifth anchor is toggled when its checkpoint is scrolled past', function() {
      // Scroll to the third element and expect 2 to no longer be highlighted
      $j(window).scrollTop(2500)
      $j(window).trigger("scroll")
      expect(utility.$subject.eq(3).attr("data-gef-scrolltoggle-passed")).toBe("false")
      expect(utility.$subject.eq(4).attr("data-gef-scrolltoggle-passed")).toBe("true")
      expect(utility.$subject.eq(5).attr("data-gef-scrolltoggle-passed")).toBe("false")
    })

    it('last anchor is toggled when its checkpoint is scrolled past', function() {
      $j(window).scrollTop(4500)
      $j(window).trigger("scroll")
      expect(utility.$subject.eq(7).attr("data-gef-scrolltoggle-passed")).toBe("true")
    })
  })

  describe("ScrollToggle is using the top of the viewport as the checkpoint", function() {

    var utility,
    selector = "body",
    config = {
      "scroll_window"   : true,
      "scroller_position" : "top",
      "subject"         : "#subject",
      "checkpoint"      : "#checkpoint3",
    }

    beforeEach(function() {
      loadFixtures("utilities/scrollToggle.html")
      //spyOn(ScrollToggle.prototype, 'scrollTestMulti').and.callThrough()
      utility = new ScrollToggle(selector, config)
    })

    it("has set the scroll position to top", function() {
      expect(utility.config.scroller_position).toBe("top")
    })

    it("hasn't toggled the subject once the checkpoint has passed bottom of viewport", function() {
      // configuration has set the window height to 768
      $j(window).scrollTop(10000-700)
      $j(window).trigger("scroll")
      expect(utility.$subject.attr(utility.config.passed_attr)).toBe("false")
    })

    it("has toggled the subject once the checkpoint has passed top of viewport", function() {
      // configuration has set the window height to 768
      $j(window).scrollTop(10001)
      $j(window).trigger("scroll")
      expect(utility.$subject.attr(utility.config.passed_attr)).toBe("true")
    })
  })

 describe("ScrollToggle is toggling a class when the checkpoint is passed", function() {

    var utility,
    selector = "body",
    config = {
      "scroll_window"   : true,
      "subject"         : "#subject",
      "checkpoint"      : "#checkpoint3",
      "toggle_class"    : ".active"
    }

    beforeEach(function() {
      loadFixtures("utilities/scrollToggle.html")
      utility = new ScrollToggle(selector, config)
    })

     it("has added the class once the checkpoint has passed", function() {
      // configuration has set the window height to 768
      $j(window).scrollTop(10000-500)
      $j(window).trigger("scroll")
      expect(utility.$subject).toHaveClass(config.toggle_class)
    })
  })

  describe("ScrollToggle is passed a percentage for the viewport checkpoint position", function() {
    var utility,
    selector = "body",
    config = {
      "scroll_window"     : true,
      "scroller_position"   : "percent",
      "scroller_percent"  : 25,
      "subject"           : "#subject",
      "checkpoint"        : "#checkpoint3"
    }

    beforeEach(function() {
      loadFixtures("utilities/scrollToggle.html")
      spyOn(ScrollToggle.prototype, 'setOffset').and.callThrough()
      utility = new ScrollToggle(selector, config)
    })

    it("config.scroller_position is set to the correct option", function() {
      expect(utility.config.scroller_position).toBe("percent")
    })

    it("has set the correct value to config.scroller_percent", function() {
      expect(utility.config.scroller_percent).toBe(config.scroller_percent)
    })

    it("the output of setOffset should match", function() {
      var offset_top = 2000,
          viewport_height = $j(window).height(), // set by config
          expectation = offset_top - (viewport_height/100) * config.scroller_percent

      expect(utility.setOffset(offset_top)).toBe(expectation)
    })

    it("config.scroller_position defaults to bottom because no percentage was passed", function() {
      config.scroller_percent = undefined
      utility = new ScrollToggle(selector, config)
      expect(utility.config.scroller_position).toBe("bottom")
    })
  })
}

define(["/base/dist/assets/js/utilities/scrollToggle.js"], scrollToggleSpec)