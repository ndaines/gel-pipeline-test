"use strict"

var ariaTogglerSpec = function(AriaToggler) {

  describe("AriaToggler is instantiated:", function() {
    var utility,
        selector = "[data-gef-tabs]",
        tabs     = "[role=tab]",
        tabpanels     = "[role=tabpanel]"

    beforeEach(function() {
      loadFixtures("utilities/ariaToggler.html")
      spyOn(AriaToggler.prototype, 'createEvents').and.callThrough()
      utility = new AriaToggler(selector)
    })

    it("calls the function createEvents()", function() {
      expect(AriaToggler.prototype.createEvents).toHaveBeenCalled()
    })

    it("applies the correct state changes to the tab when a tab is triggered", function() {
      $j(tabs).eq(1).click()
      expect($j(tabs).eq(1).attr("aria-expanded")).toBe("true")
    })

    it("applies the correct state changes to the tabpanel when a tab is triggered", function() {
      $j(tabs).eq(1).click()
      expect($j(tabpanels).eq(1).attr("aria-hidden")).toBe("false")
    })
  })

  describe("AriaToggler is instantiated on a component which is in a default 'triggered' state:", function() {
    var utility,
        selector = "[data-gef-notice-ribbon]",
        tabs     = "#noticeRibbon-tab",
        tabpanels     = "#noticeRibbon-content"

    beforeEach(function() {
      loadFixtures("utilities/ariaToggler.html")
      utility = new AriaToggler(selector)
    })

    it("expects the component to be triggered", function() {
      expect(utility.expanded).toBe(true)
      expect($j(tabs).attr("aria-expanded")).toBe("true")
    })

    it("expect to be un-triggered onced clicked", function() {
      $j(tabs).click()
      expect($j(tabpanels).attr("aria-hidden")).toBe("true")
    })
  })

  describe("AriaToggler is instantiated to open the first pair by via config", function() {
    var utility,
        selector = "[data-gef-tabs]",
        tabs     = "[role=tab]",
        tabpanels     = "[role=tabpanel]",
        config = {
          auto_open: true
        }

    beforeEach(function() {
      loadFixtures("utilities/ariaToggler.html")
      utility = new AriaToggler(selector, config)
    })

    it("expecting the first set to be open", function() {
      expect($j(tabs).eq(0).attr("aria-expanded")).toBe("true")
      expect($j(tabpanels).eq(0).attr("aria-hidden")).toBe("false")
    })
  })

}

define(["/base/dist/assets/js/utilities/ariaToggler.js"], ariaTogglerSpec)
