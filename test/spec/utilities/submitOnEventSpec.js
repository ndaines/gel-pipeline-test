"use strict"

var submitOnEventSpec = function(SubmitOnEvent) {


  describe("SubmitOnEvent is initialised", function() {

    var utility,
        selector  = "#test-button"

    beforeEach(function() {
      loadFixtures("utilities/submitOnEvent.html")
      utility = new SubmitOnEvent(selector)
    })

    it("expects defaults to have been set", function() {
      expect(utility.config.triggers).not.toBeNull()
      expect(utility.config.submitForm).not.toBeNull()
    })
  })

  describe("SubmitOnEvent is initialised with config", function() {
    var utility,
        selector = "#external-button",
        config = {
          "triggers"   : "click",
          "submitForm" : ".gef-form",
          "test"       : true
        }
    beforeEach(function() {
      loadFixtures("utilities/submitOnEvent.html")
      utility = new SubmitOnEvent(selector, config)
    })

    it("expects defaults to have been overwritten", function() {
      expect(utility.config.triggers).toEqual(config.triggers)
      expect(utility.config.submitForm).toEqual(config.submitForm)
    })
  })

  describe('Trigger calls the submitForm function', function() {
    var utility,
        spyEvent,
        selector = "#external-button",
        config = {
          "triggers"   : "click",
          "submitForm" : ".gef-form",
          "test"       : true
        }

    beforeEach(function() {
      loadFixtures("utilities/submitOnEvent.html")
      spyOn(SubmitOnEvent.prototype, 'submitForm').and.callThrough()
      utility = new SubmitOnEvent(selector, config)
    })

    it('should call submitForm for click trigger', function() {
      spyEvent = spyOnEvent(selector, config.triggers);
      // test click
      $j(selector).trigger(config.triggers)
      expect(config.triggers).toHaveBeenTriggeredOn(selector);
      expect(spyEvent).toHaveBeenTriggered();
      expect(SubmitOnEvent.prototype.submitForm).toHaveBeenCalled();
    })

  })

  describe('A radio button is checked', function() {
    var utility,
    selector = "#checked-test",
    config = {
      "triggers"   : "change",
      "submitForm" : ".gef-form",
      "test"       : true
    }

    beforeEach(function() {
      loadFixtures("utilities/submitOnEvent.html")
      spyOn(SubmitOnEvent.prototype, 'submitForm').and.callThrough()
      utility = new SubmitOnEvent(selector, config)
    })

    it('should call submitForm when clicked', function() {
      $j(selector).click()
      expect(SubmitOnEvent.prototype.submitForm).toHaveBeenCalled();
    })
  })

  describe('An option is selected', function() {
    var utility,
    selector = "#selected-test",
    config = {
      "triggers"   : "change",
      "submitForm" : ".gef-form",
      "test"       : true
    }

    beforeEach(function() {
      loadFixtures("utilities/submitOnEvent.html")
      spyOn(SubmitOnEvent.prototype, 'submitForm').and.callThrough()
      utility = new SubmitOnEvent(selector, config)
    })

    it('should call submitForm when selected', function() {
      $j('.option-first').attr('selected', 'selected')
      $j(selector).change()
      expect(SubmitOnEvent.prototype.submitForm).toHaveBeenCalled();
    })
  })

  describe('A keystroke trigger is passed with the specific keys', function() {
    var utility,
    selector = "#external-button",
    config = {
      "triggers"   : "keypress",
      "keys"       : {"keypress": "return", "keyup": "space"},
      "submitForm" : ".gef-form",
      "test"       : true
    }

    beforeEach(function() {
      loadFixtures("utilities/submitOnEvent.html")
      spyOn(SubmitOnEvent.prototype, 'findKeystrokeEvents').and.callThrough()
      utility = new SubmitOnEvent(selector, config)
    })

    it('has run findKeystrokeEvents function and returns "return" string', function() {
      $j(selector).trigger(config.triggers)
      expect(SubmitOnEvent.prototype.findKeystrokeEvents).toHaveBeenCalledWith(config.triggers, config.keys)
      var result = SubmitOnEvent.prototype.findKeystrokeEvents(config.triggers, config.keys)
      var expected = "return"
      expect(result).toBe(expected)
    })
  })
}

define(["/base/dist/assets/js/utilities/submitOnEvent.js"], submitOnEventSpec)
