"use strict"

// ####
// WARNING: This unit test doesn't work because PhantomJS doesn't return the cookie properly
// ####


var cookieControlSpec = function(CookieControl) {

  describe("cookieControl is instantiated:", function() {
    var utility

    beforeEach(function() {
      utility = new CookieControl(null)
    })

    it("has collected all currently set cookies", function() {
      expect(utility.allCookiesString).toBe(defined)
    })
  })

  describe("cookieControl is sent a cookie to set as true:", function() {
    var utility,
        cookieName = testCookie,
        value = "TRUE"

    beforeEach(function() {
      spyOn(CookieControl.prototype, 'returnCookie')
      spyOn(CookieControl.prototype, 'setCookie')
      utility = new CookieControl(config)

      var fetchedCookie = utility.prototype.returnCookie()

    })
  })

  it("looks to see if cookie has been applied", function() {
    expect(fetchedCookie(cookieName)).toBe(false)
    expect(utility.cookieApplied).toBe(false)
  })

  it("sets the cookie when the setCookie function is called", function() {
    utility.prototype.setCookie(cookieName, value, 5)
    expect(utility.prototype.setCookie).toHaveBeenCalled()
    expect(utility.cookieApplied).toBe(true)
  })

  it("returns the value of the cookie when returnCookie is called", function() {
    expect(fetchedCookie(cookieName)).toBe("TRUE")
  })
}

define(["/base/dist/assets/js/utilities/cookieControl.js"], cookieControlSpec)