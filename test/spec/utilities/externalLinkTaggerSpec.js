"use strict"

var externalLinkTaggerSpec = function(ExternalLinkTagger) {

  describe("When a ExternalLinkTagger has been initialised without being passed a selector or config", function() {
    var externalLinkTagger

    beforeEach(function() {

      loadFixtures("utilities/externalLinkTagger.html")

      spyOn(ExternalLinkTagger.prototype, 'setConfig').and.callThrough()

      externalLinkTagger = new ExternalLinkTagger()
    })

    it("setConfig() is called when a ExternalLinkTagger is initialised", function() {
      expect(ExternalLinkTagger.prototype.setConfig).toHaveBeenCalled();
    })

    it("anchor elements have not been tagged as external links", function() {
      expect($j("#jasmine-fixtures a")).not.toContainElement("span.gef-external-link")
    })

  })


}


define(["/base/dist/assets/js/utilities/externalLinkTagger.js"], externalLinkTaggerSpec)
