"use strict"

var inputValueTransferrerSpec = function(InputValueTransferrer) {


  describe("When InputValueTransferrer is instantiated,", function() {
    var component,
        $object,
        selector = "[data-gef-input-transferrer]"

    beforeEach(function() {
      loadFixtures("utilities/inputValueTransferrer.html")
      $object = $j(selector)
      spyOn(InputValueTransferrer.prototype, 'setChangeEvents').and.callThrough()
      component = new InputValueTransferrer(selector)
    })

    it("it expects the selectors to match", function() {
      expect(component.config.selector).toBe(selector)
    })

    it("it has found the required input elements(s)", function() {
      expect(component.$inputs.length).not.toBe(0)
    })

    it("it has found the required receiver element", function() {
      expect(component.$receiver.selector).toBe("#receiver")
    })

    it("it has set the change events", function() {
      expect(InputValueTransferrer.prototype.setChangeEvents).toHaveBeenCalled()
    })
  })

  describe("When InputValueTransferrer has its inputs interacted with,", function() {
     var component,
        $object,
        selector = "[data-gef-input-transferrer]"

    beforeEach(function() {
      loadFixtures("utilities/inputValueTransferrer.html")
      $object = $j(selector)
      spyOn(InputValueTransferrer.prototype, 'setReceiverValue').and.callThrough()
      component = new InputValueTransferrer(selector)
    })

    it("it will has called setReceiverValue", function() {
      $object.eq(0).trigger("change")
      expect(InputValueTransferrer.prototype.setReceiverValue).toHaveBeenCalled()
    })

    it("it will have set a value to the receiver", function() {
      $object.eq(0).trigger("change")
      expect($j("#receiver").val()).not.toEqual('')
    })
  })

  describe("When InputValueTransferrer has a text input changed,", function() {
     var component,
        $input,
        selector = "[data-gef-input-transferrer]"

    beforeEach(function() {
      loadFixtures("utilities/inputValueTransferrer.html")
      $input = $j("#input3")
      component = new InputValueTransferrer(selector)
    })

    it("if will have set the receiver value to the input value", function() {
      $input.val("bazooka").trigger("change")
      expect($j("#receiver").val()).toEqual($input.val())
    })

  })
}

define(["/base/dist/assets/js/utilities/inputValueTransferrer.js"], inputValueTransferrerSpec)