"use strict"
import NotificationCentre from "core/notificationCentre"
import CookieControl from "utilities/cookieControl"
import * as $ from "jquery"

/**
 * The User module exposes user details stored in cookies (set when a user logs in). While this class can be instantiated as separate  instance objects it is really designed to be used in GEF as a global singleton
 *
 * @since 1.7.0
 *
 * @author Alex Motyka <alex.motyka@det.nsw.edu.au>, 
 * @copyright © 2019 State Government of NSW 2016
 *
 * @class
 */
class User {

  /**
   * Constructor for User
   *
   * @constructor
   *
   * @param {String|Element|jQuery} [selector] - Either a CSS selector, DOM element or matched jQuery object
   * @param {Object} config - class configuration options
   *
   * @example
   * // Instantiate a new DiagnosticHUD
   * let diagnosticHUD = new DiagnosticHUD(".gef-global-header", {})
   */
  constructor(selector, config) {

    // Check if only one argument has been passed
    if (arguments.length === 1) {
      // If argument[0] is a config object then set the config arg and nullify the selector arg
      if (selector instanceof Object && !(selector instanceof $)) {
        config = selector
        selector = null
      }
    }

    // Deffault config options
    this.config = {
      selectors: {
        pageTitle: "title",
        divisionTitle: ".gef-section-header__logo a"
      },
      cookies: {
        userID: "UserID",
        userBrowsingHistory: "UserBrowsingHistory",
        userDisplayName: "UserDisplayName",
        userRoles: "UserRoles"
      }
    }

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }

    if (selector) {
      this.$container = $(selector)
    }

    // Set up a instance level reference to the NotificationCentre singleton
    this.notificationCentre = NotificationCentre.shared()

    this.cookieController = new CookieControl()

    // Get the user's browsing history from the UserBrowsingHistory cookie
    let browsingHistoryCookie = this.cookieController.returnCookie(this.config.cookies.userBrowsingHistory)
    let browsingHistoryArray = JSON.parse(decodeURIComponent(browsingHistoryCookie))

    // Add the user's browsing history stored in the UserBrowsingHistorycook to instance history property
    if (browsingHistoryArray) {
      this.history = browsingHistoryArray
    } else {
      this.history = []
    }

    // Get the current current 
    let pageTitle = $(this.config.selectors.pageTitle).text().split("|")[0].trim()
    let pageURL = document.location.href

    // Grab the division name and URL from the page header
    let $divisionNameAnchor = $(this.config.selectors.divisionTitle)

    if ($divisionNameAnchor.length > 0) {
      // Add the page to the history array
      this.addPage(pageTitle, pageURL, $divisionNameAnchor.first().text(), $divisionNameAnchor.first().attr("href"))
    }


    // Hashed user's DETUserGUID
    let idCookie = document.cookie.match(new RegExp(`${this.config.cookies.userID}=([^;]+)`))

    if (idCookie) {
      this.id = idCookie[1]
    }

    // User's displayname e.g. "John Doe"
    let displayName = document.cookie.match(new RegExp(`${this.config.cookies.userDisplayName}=([^;]+)`))
    if (displayName) {
      this.displayName = displayName[1]
    }

    // User's Active Directory detAttribute15 (i.e. their role, not to be confused with title). Can have multiple.
    let rolesCookie = document.cookie.match(new RegExp(`${this.config.cookies.userRoles}=([^;]+)`))
    if (rolesCookie) {
      this.roles = decodeURI(rolesCookie[1]).split("|")
    }

    // Diagnostic update
    this.notificationCentre.publish("diagnosticUpdate", {
      module: "User",
      messages: [
        { text: this.config.cookies.userID, variable: this.id },
        { text: this.config.cookies.userRoles, variable: this.roles },
        { text: this.config.cookies.userDisplayName, variable: this.displayName }
      ]
    })

  }

  /**
   * Add a page to the user's history
   * 
   * @param {String} pageTitle - Page title
   * @param {String} pageUrl - Page URL
   * @param {String} divisionName - Division name
   * @param {String} divisionUrl - Division URL
   */
  addPage(pageTitle, pageUrl, divisionName, divisionUrl) {

    let historyItem = {
      title: pageTitle,
      url: pageUrl,
      division: {
        title: divisionName,
        url: divisionUrl
      },
      created: (new Date()).toISOString()
    }
    
    // Add the new history item to the begining of the array
    this.history.unshift(historyItem)

    // If the array has more than 10 items drop the last item
    if (this.history.length > 10) {
      this.history.pop()
    }

    // Update the UserBrowsingHistory cookie with new array
    this.cookieController.setCookie(this.config.cookies.userBrowsingHistory, encodeURIComponent(JSON.stringify(this.history)), 31536000)

    return this.history
  }


  /**
   * Static function to instantiate the User class as singleton
   *
   * @static
   *
   * @param {String|Element|jQuery} selector - Either a CSS selector, DOM element or matched jQuery object
   * @param {object} config   - class configuration arguments. Refer to class constructor for complete documentation of the config object
   *
   * @returns {User} Reference to the same User instantiated in memory
   */
  static shared(selector, config) {
    return this.instance != null ? this.instance : this.instance = new User(selector, config)
  }

}

/**
 * Exports the tabpanelPersonalisation class as a JS module
 * @module
 */
export default User