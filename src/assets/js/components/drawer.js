import  classManipulator  from '/classManipulator.js';

new classManipulator(".gel-drawer a", {
  "event_type": "click",
  "subject": ".gel-drawer",
  "toggle": true,
  "click_outside": true,
  "click_outside_ignores": ".gel-drawer div" });
