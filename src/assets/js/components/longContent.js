"use strict"

import KeyCodes from "keycodes"
import ScrollToggle from "/scrollToggle.js"
import AnchorIndexHighlight from "/anchorIndexHighlight.js"
/**
* anchorIndex.js - Builds a table of contents anchor index based on HTML content

* @class
* @requires jQuery
*/

class AnchorIndex {

  /**
  * Creates a new AnchorIndex
  *
  * @constructor
  *
  * @param {String}   selector - the jQuery selector in question
  * @param {Object}   config - class configuration options. Options vary depending on need
  * @param {String}   [config.$container = $(selector)] - the object the AnchorIndex crawls to create the anchors
  * @param {String}   [config.$index = $('.gel-LD-index')] - the object to inject the AnchorIndex pattern into
  * @param {String}   [config.targets = "h2, h3"] - the selector to build the anchors from
  * @param {String}   [config.template.html = undefined] - the html structure that will be used to build the AnchorIndex
  * @param {String}   [config.template.selector = ".gel-anchor-index"] - the selector of the html structure
  * @param {String}   [config.template.list_class = "gel-link-list"] - the list pattern to inject into the AnchorIndex
  * @param {int}      [config.mobileWidth]   - Maximum width of window to make changes
  */

  constructor(selector, config) {
    this.config = {
      $container    : $(selector),
      $index        : $('.gel-LD-index'),
      targets       : "h2, h3",
      template      :  {
        html           : undefined,
        selector       : ".gel-anchor-index",
        list_class     : "gel-link-list"
      },
      mobileWidth: "767px"
    }

    // Check if config has been passed to constructor
    if (config) {
      // Merge default config with passed config
      this.config = $.extend(true, {}, this.config, config)
    }

    // Save the container for later
    this.$container = this.config.$container

    // Save the index container for later
    this.$index = this.config.$index

    // Set the template if it hasn't been passed via the config
    if (this.config.template.html === undefined) {
      this.config.template.html = '<nav class="gel-anchor-index" role="navigation"></nav>'
    }

    // Set class variable(s)
    this.$mql = window.matchMedia(`(max-width: ${this.config.mobileWidth})`)

    this.initAnchorIndex()

    // Allow toggling between desktop and mobile layout
    var component = this
    $(window).resize( function() {
      component.initAnchorIndex()
    });

    window.addEventListener('hashchange', this.handleHashChange);
  }

  /**
  * Instantiates the index and UI events
  *
  * @initAnchorIndex
  *
  */
  initAnchorIndex() {

    // find the targets
    this.$targets = this.$container.children(this.config.targets)

    // build the anchor index layout based on breakpoint
    if (this.$mql.matches) {
      // creates select box version of index
      this.buildAnchorIndex('select')
    } else {
      // creates anchor link version of index
      this.buildAnchorIndex('link')
    }

    // bind select options to anchors
    this.bindSelectToAnchors()

    // create the back to index links
    this.backToIndex()

    // resize anchor index to prevent overlap with footer
    this.resizeIndex()

    // hide select index nav to prevent overlap with footer
    this.mobileHideStickyIndex()

    // instantiate anchor highlighting
    this.anchorHighlight = new AnchorIndexHighlight(this.$index)

    // alert index is generated at the end
    this.$index.prepend('<span role="alert" class="show-on-sr">Index is generated</span>')
  }

  /**
  * constructs the Anchor index
  * depending on nav_type, it build anchors links or a select box
  *
  * @buildAnchorIndex
  *
  */
  buildAnchorIndex(nav_type) {
    var component = this

    // add the anchor index
    if (this.$index.length) {
      this.$index.empty().prepend(this.config.template.html)
    } else {
      // Can't place
      console.warn("Couldn't find where to place the AnchorIndex")
    }

    // create the link list container
    var container = ''
    if (nav_type === 'link') {
      container = `<h2 id="gel-anchor-index" class="uk-h6 gel-anchor-box__title" tabindex="0">On this page</h2><ul class="${this.config.template.list_class}"></ul>`
    } else if (nav_type === 'select') {
      container =
      `<form class="gel-form" action method="get">
        <label id="gel-anchor-index" for="gel-anchor-index-select" class="uk-h6 gel-anchor-box__title">Select section</label>
        <select id="gel-anchor-index-select" name="gel-anchor-index-select" class="${this.config.template.list_class}">
        </select>
        <button class="uk-button gel-button" type="button" aria-label="Go to section">Go to section</button>
      </form>`
    } else {
      // Can't create layout container
      console.warn("Couldn't determine layout container type for AnchorIndex")
    }

    this.$index.find(this.config.template.selector).append(container)

    // define base entry point
    var li_entry = '.' + this.config.template.list_class

    // loop through targets and add them to the index
    this.$targets.each( function(i) {
      // create entry point for link list
      var previous_target        = component.$targets[i-1],
          current_heading_level  = component.headingLevel($(this)),
          previous_heading_level = previous_target ? component.headingLevel($(previous_target)) : current_heading_level

      // add list entry point and update entry point
      if (current_heading_level > previous_heading_level) {
        component.$index.find('li:last').append('<ul id="gel-ul-' + i + '"></ul>')
        li_entry = '#gel-ul-' + i
      }

      // restore entry point for higher level heading
      if (current_heading_level < previous_heading_level) {
        for (var j = 0; j < previous_heading_level - current_heading_level; j++) {
          li_entry = $(li_entry).parentsUntil('ul').parent('ul')
        }
      }

      // add an ID
      var anchor_name = component.anchorName($(this), i)
      var anchor_name_escaped = anchor_name.replace(/(['"&:;])/g, "\\$1");

      $(this).attr('id', anchor_name)
      $(this).attr('tabindex', '0')

      // add item to the link list
      if (nav_type === 'link') {
        if ($(this).is('h2')) {
          component.$index.find(li_entry).append(
            '<li><i aria-hidden="true"></i><a href="#' + anchor_name_escaped + '"data-h2-index="' + anchor_name + '"id="back_' + anchor_name + '"'  + '>' + $(this).html() + '</a></li>'
          )
        } else {
          component.$index.find(li_entry).append(
            '<li><a href="#' + anchor_name_escaped + '">' + $(this).html() + '</a></li>'
          )
        }
      } else if (nav_type === 'select') {
        var base_heading_level = component.headingLevel($(component.$targets[0])),
            depth = Math.max(0, current_heading_level - base_heading_level)

        component.$index.find('.' + component.config.template.list_class).append(
          '<option value="#' + anchor_name + '">' + "&nbsp".repeat(depth * 3) + $(this).html() +  '</option>'
        )
      }
    })
  }

  /**
  * enable anchor behaviour using select options
  *
  * @backToIndex
  *
  */
  bindSelectToAnchors() {
    var $selectButton = this.$index.find('button')

    // Reset bind prior to binding change event
    $selectButton.off('click')

    // Bind select to trigger jump to index
    var component = this

    $selectButton.on('click', function() {
      var sticky_padding = component.$index.css('padding-top').replace('px',''),
          selected_option = component.$index.find('option:selected').val(),
          sticky_nav_height

      // Prevent heading from being overlapped by sticky index
      if (sticky_padding == 0) {
        sticky_nav_height = $selectButton.closest('.gel-anchor-index').outerHeight() * 0.75
      }  else {
        sticky_nav_height = $selectButton.closest('.gel-anchor-index').outerHeight()
      }

      if (sticky_nav_height) {
        window.scrollTo(0, $(selected_option).offset().top - sticky_nav_height)
        // Focus on the heading after select option selected
        $selectButton.blur()
        $(selected_option).click().focus()
      }
    })
  }

  /**
  * enable back to index to focus on active index anchor
  *
  * @backToIndex
  *
  */
  backToIndex() {
    var component = this

    // create back to index links
    if ($('[data-gel-h2-main]').length === 0) {
      var heading_text = '',
          heading_link = ''

      this.$targets.each( function(n) {
        if (component.$targets.first('h2').text() === $(this).text()) {
          // Store the first h2 text and link for the back to index link
          heading_text = $(this).text()
          heading_link = component.anchorName($(this), n)
          return
        }

        if ($(this).is('h2')) {
          $('<a class="gel-skiplink__link" href="#back_'+ heading_link + '" tabindex="0" aria-label="Back to index - ' + heading_text + '" data-gel-h2-main="' + heading_link + '" tabindex="0">Back to Index</a>').insertBefore($(this))
          // Update heading text and link for the next h2 section
          heading_text = $(this).text()
          heading_link = component.anchorName($(this), n)
        }
      })
    }

    // Remove previous triggers on back to index before binding new trigger
    $('[data-gel-h2-main]').off('keydown, click')

    // Bind back to index functionality
    $('[data-gel-h2-main]').on('keydown', function(event) {
      if(event.which === KeyCodes.return || event.keyCode === KeyCodes.return) {
        var h2_index = $(this).attr('data-gel-h2-main')

        // setTimeout to resolve Firefox .focus() bug
        setTimeout( function() {
          component.$index.find('[data-h2-index="' + h2_index + '"]').focus()
        }, 100)
      }
    }).on('click', function() {
      component.$index.find('select').focus().click()
    })
  }

  /**
  * resize the index container to prevent overlap with the footer on sticky
  *
  * @backToIndex
  *
  */
  resizeIndex() {
    var $window = $(window),
        $sticky_container = this.$index.find(this.config.template.selector)

    $window.scroll( function() {
      var height_difference = parseInt($window.scrollTop() + $window.height()) - $('footer').offset().top
      if (height_difference > 0) {
        $sticky_container.css('max-height', $window.height() - height_difference)
        // Keep the index position at the bottom as index height changes
        $sticky_container.scrollTop($sticky_container.find('ul').outerHeight())
      } else {
        if ($sticky_container.attr('style')) {
          $sticky_container.removeAttr('style')
        }
      }
    })
  }

  /**
  * hide the sticky select nav index to prevent overlap with the footer
  *
  * @mobileHideStickyIndex
  *
  */
  mobileHideStickyIndex() {
    var $select = this.$index.find('select'),
        $selectNav = $select.closest('.gel-anchor-index'),
        select_nav_height = $selectNav.outerHeight(),
        $window = $(window)

    $window.scroll( function() {
      if ($select.length && ($window.scrollTop() > $('footer').offset().top - select_nav_height)) {
        $selectNav.addClass('hide')
      } else {
        $selectNav.removeClass('hide')
      }
    })
  }

  /**
  * evaluated heading tag level
  *
  * @headingLevel
  *
  */
  headingLevel($target) {
    if ($target && $target.is(':header')) {
      return $target.prop('tagName').split('').pop()
    } else {
      return 0
    }
  }

  /**
  * creates a unique anchor based on the html and an id
  *
  * @anchorName
  *
  */
  anchorName($target, id) {
    return $target.text().replace(/[\d.\d]+[ ]/g, '').replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g,"").split(' ')[0] + '_' + id
  }

  getHashFromUrl = (url = '') => {
    const hashLocation = url.indexOf('#');
    if (hashLocation === -1) { return ''; }
    return url.slice(hashLocation + 1);
  }

  handleHashChange = (event) => {
    const { newURL, oldURL } = event;
    const hash = this.getHashFromUrl(newURL || location.href);
    if (hash === 'gel-anchor-index') {
      if (oldURL) {
        window.history.replaceState({}, undefined, oldURL);
      }

      const $select = this.$index.find('select');
      if ($select) {
        $select.focus();
      }
    }
  }
}

new AnchorIndex(".gel-LD-main")

