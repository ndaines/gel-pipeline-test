window.onload = function() {
  if(location.hash){
    var elId = location.hash.replace('#','');
    var scrollToEl = document.getElementById(elId);
    scrollToEl.scrollIntoView(true);
  }
}
jQuery(document).ready(function ($) {
  var scrollToEl = $(".gel-skiplink__link");
  scrollToEl.keydown(function(e) {
    if (e.which === 13 || e.which === 8) {
      e.preventDefault();
      var elId = this.hash.replace('#','');
      var scrollToEl = document.getElementById(elId);
      console.log(scrollToEl);
      scrollToEl.scrollIntoView(true);
      scrollToEl.focus();
    }
  });
})
