"use strict"
import moment from "moment"
import doT from "dot"
import postTemplate from './postTemplate';

/**
 * Stores social media post data
 *
 * @since 1.1.31
 *
 * @author Digital Services <communications@det.nsw.edu.au>
 * @copyright © 2015 State Government of NSW 2015
 *
 * @class
 *
 * @requires momment:vendor/moment/moment
 */
class Post {

  /**
   * Post constructor
   *
   * @constructor
   *
   * @param {Object} rawPost - The raw social media post returned from the Social Media Aggregator microservice
   * @param {boolean} posttruncate
   * @example
   * // Instantiate a new Post
   * let Post = new Post(rawPost)
   */
  constructor(rawPost) {

    if (rawPost) {
      this.processRawPost(rawPost)
    }
  }


  /**
   * Takes a raw social media post returned by the Social Media Aggregator and maps it to Post properties
   *
   * @param {Object} rawPost - The raw social media post returned from the Social Media Aggregator microservice
   */
  processRawPost(rawPost) {
    this.rawPost = rawPost

    this.service = rawPost.accountType
    this.user = {
      username: rawPost.username,
      name: rawPost.screenName,
      avatar: rawPost.avatarUrl,
      url: rawPost.userUrl
    }
    this.published = rawPost.rawTimestamp
    this.content = {
      photo: rawPost.thumbnail,
      text: rawPost.text,
    }
    this.url = rawPost.url

    // If the post doesn't already have an ID then in the case of YouTube and Vimeo posts use the video ID as the post ID
    if (rawPost.postId) {
      this.id = rawPost.postId
    } else if (rawPost.accountType == "vimeo" || rawPost.accountType == "youtube") {
      this.id = this.extractVideoID(rawPost.url)
    }
  }


  /**
   * Takes a Vimeo or YouTube video URL and returns the video ID
   *
   * @param {String} url - A video URL
   *
   * @returns {String} - A YouTube or Vimeo video ID
   */
  extractVideoID(url) {
    // If the URL is from YouTube
    if (url.match(/youtube/)) {
      return url.match(/\?v=([^&]*)/)[1]
    }
    // If the URL is from Vimeo
    if (url.match(/vimeo/)) {
      return url.match(/\https:\/\/vimeo.com\/([^&]*)/)[1]
    }
  }



  /**
   * Returns a YouTube/Vimeo iframe embed code compatible URL
   *
   * @returns {String} - The URL needed by the YouTube/Vimeo iframe embed code
   */
  embedUrl() {
    // If the URL is from YouTube
    if (this.url.match(/youtube/)) {
      return `https://www.youtube.com/embed/${this.id}?rel=0`
    }
    // If the URL is from Vimeo
    if (this.url.match(/vimeo/)) {
      return `https://player.vimeo.com/video/${this.id}?rel=0`
    }

  }

  /**
   * Return's a Post as HTML string that conforms to the gel Social Media Post component spec
   *
   * @param {Number} contentLength - (optional) The number of characters the text content of the post should be trimed to
   * @param {Boolean} minimalist - (optional) Set to true to hide the majority of post metadata
   * @param {Boolean} dateplacement - (optional) Set to true to have a differet date design placement
   *
   * @returns {String} - Post as a HTML conforming to the gel Social Media Post component
   */
  toHTML(contentLength, minimalist, dateplacement) {
    let post = this

    // If contentLength is specified then reduce the size of the text content
    if (post.content.text) {
      var posttruncate;
      if (contentLength) {
        // If contentLength is specified then reduce the size of the text content
        if (post.content.text.length > contentLength) {
          posttruncate = true;
        }
        post.content.text = post.content.text.substring(0, contentLength)

        //find if text has url if yes then wrap ahref around it
        post.content.text = post.content.text.replace(/\bhttp[^ ]+/ig, urlwrapinpost)
        function urlwrapinpost(poststr) {
          return '<a href="' + poststr + '">' + poststr + '</a>';
        }
        //add truncate style only if the post is truncated
        if (posttruncate) {
          post.content.text = post.content.text.substr(0, Math.min(post.content.text.length, post.content.text.lastIndexOf(" ")))
          post.content.text = post.content.text.replace(/\s*$/, "") + `…<span class="show-on-sr">post truncated</span>`
        }
      }
    }

    if (post.service) {
      if (String.prototype.capitalize = function () {
        return this.charAt(0)
          .toUpperCase() + this.slice(1);
      });

    }
    let templateFn = doT.template(postTemplate)

    let mediaHTML = templateFn({
      post: post,
      moment: moment,
      minimalist: minimalist,
      dateplacement: dateplacement
    })

    return mediaHTML
  }


  /**
   * Returns a string representation of the SocialMediaAccount object
   *
   * @returns {String} - Post object as JSON string
   */
  toString() {
    return JSON.stringify(this)
  }

}


/**
 * Exports the Post class as a module
 * @module
 */
export default Post
