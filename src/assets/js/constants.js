// Bootstrap
export const BOOTSTRAP_UTIL_DISPLAY_NONE = 'd-none';

// Guided Journey
export const GEL_GUIDEDJOURNEY_NAME = 'Guided Journey';
export const GEL_GUIDEDJOURNEY_LOADING_MESSAGE = 'Please wait while page is being generated';
export const GEL_GUIDEDJOURNEY_ERROR_MESSAGE = 'There has been a problem , please log a ticket in Zendesk';

// Guided Journey / Component
export const GEL_GUIDEDJOURNEY_ROOT = '.gel-main--GJ';
export const GEL_GUIDEDJOURNEY_CONTENT = '.gel-guided-journey__content';
export const GEL_GUIDEDJOURNEY_LANDING = '.gel-guided-journey__landing';
export const GEL_GUIDEDJOURNEY_PAGE = '.gel-guided-journey__page';
export const GEL_GUIDEDJOURNEY_ASSET_ID = 'data-gel-asset-id';

export const GEL_GUIDEDJOURNEY_SIDENAV = '.sn__guided-journey';
export const GEL_GUIDEDJOURNEY_MOBILENAV = '#gel-journey-index-select';
export const GEL_GUIDEDJOURNEY_MOBILENAV_FORM = 'form';
export const GEL_GUIDEDJOURNEY_MOBILENAV_SELECT = '#gel-journey-index-select';

export const GEL_PAGINATION_NEXT = '.gel-pagination__next';
export const GEL_PAGINATION_PREV = '.gel-pagination__prev';
