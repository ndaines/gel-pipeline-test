"use strict"

module.exports = function(gulp, plugins, options, errorHandler) {
  return function(callback) {

    plugins.requirejs({
      baseUrl: options.paths.build.assets.js + "apps/search/",
      name: "searchSPA",
      out: "searchSPA.js",
      paths: {
        "apps": "../../apps",
        "jquery": "empty:",
        "angular": "../../vendor/angular/angular",
        "angularSanitize": "../../vendor/angular-sanitize/angular-sanitize",
        "angularUtilsPaginate": "../../vendor/angular-utils-pagination/dirPagination"
      },
      shim: {
        "angular": {
          exports: "angular"
        },
        "angularSanitize": ["angular"],
        "angularUtilsPaginate": ["angular"]
      }
    })

    // If the --compress flag is true then compress the JS
    .pipe(plugins.if(options.compress, plugins.uglify()))

    .on("error", errorHandler)

    .pipe(gulp.dest(options.paths.build.assets.js + "apps/search/"))

    callback()
  }
}
