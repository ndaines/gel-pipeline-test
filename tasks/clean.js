"use strict"

module.exports = function(gulp, plugins, options) {
  return function() {
    return gulp.src('dist/', {read: false})
      .pipe(plugins.clean())
  }
}

