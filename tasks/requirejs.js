"use strict"

module.exports = function(gulp, plugins, options, errorHandler) {
  return function() {

    return plugins.requirejs({
      baseUrl: options.paths.build.assets.js + "apps/search/",
      name: "main",
      out: "main.built.js",
      paths: {
        "apps": "../../apps",
        "jquery": "empty:",
        "angular": "../../vendor/angular/angular",
        "angularSanitize": "../../vendor/angular-sanitize/angular-sanitize",
        "angularUtilsPaginate": "../../vendor/angular-utils-pagination/dirPagination"
      },
      shim: {
        "angular": {
          exports: "angular"
        },
        "angularSanitize": ["angular"],
        "angularUtilsPaginate": ["angular"]
      }
    })
    .on("error", errorHandler)
    .pipe(gulp.dest(options.paths.build.assets.js + "apps/search/"))

  }
}
