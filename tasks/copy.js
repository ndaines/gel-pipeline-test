"use strict"

module.exports = function(gulp, plugins, options, errorHandler) {
  function copy(source, destination, rename) {
    if (rename) {
      return gulp.src(source).pipe(plugins.rename(rename)).on("error", errorHandler).pipe(gulp.dest(destination));
    }
    return gulp.src(source).pipe(gulp.dest(destination));
  }

  function copyWithMiddleware(source, destination, middleware) {
    gulp.src(source).pipe(middleware).pipe(gulp.dest(destination));
  }

  function copyNodeModule(source, destination, rename) {
    return copy(options.paths.nodeModules + source, destination, rename);
  }


  return function() {
    const { nodeModules, build, dev } = options.paths;

    // jQuery
    copyNodeModule("jquery/dist/" + (options.compress ? "jquery.min.js" : "jquery.js"), build.assets.js + "vendor", "jquery.js");

    // UIKit
    var uikitPaths;
    if (options.compress) {
      uikitPaths = [nodeModules + "uikit/js/**/*.min.js"]
    } else {
      uikitPaths = [nodeModules + "uikit/js/**/*.js", "!" + nodeModules + "uikit/js/**/*.min.js"]
    }

    copy(
      uikitPaths,
      build.assets.js + "vendor/uikit",
      function(path) {
        path.basename = path.basename.replace(".min", "")
      }
    );

    // Font Awesome
    copyNodeModule("@fortawesome/fontawesome-free/**/*.{eot,svg,ttf,woff,woff2,otf}", build.assets.fonts);

    // Font Awesome Pro
    copyNodeModule("@fortawesome/fontawesome-pro/**/*.{eot,svg,ttf,woff,woff2,otf}", build.assets.fonts);

    copy(
      [nodeModules + "text-generator-pug/lorem-ipsum-mixin.pug", nodeModules + "text-generator-pug/generate-text.pug"],
      dev.assets.root + "pug/utilities/"
    );

    // RequireJS Text & JSON Plugins (JSON requires Text)
    copyWithMiddleware(
      [nodeModules + "requirejs-plugins/src/json.js", nodeModules + "requirejs-text/text.js"],
      build.assets.js + "vendor/requirejs-plugins/js",
      plugins.if(options.compress, plugins.uglify())
    );

    // Placeholder support for IE9
    copyNodeModule("jquery-placeholder/jquery.placeholder.js", build.assets.js + "vendor/jquery-placeholder");

    // Bootstrap
    copyNodeModule("bootstrap/dist/js/bootstrap.bundle.min.js", build.assets.js + "vendor/bootstrap");
    copyNodeModule("bootstrap/dist/js/bootstrap.min.js", build.assets.js + "vendor/bootstrap");

  // Momment https://momentjs.com
    copyNodeModule("moment/min/moment-with-locales.js", build.assets.js + "vendor/moment", "moment.js");

   // Jquery form validation
    copyNodeModule("jquery-validation/dist/" + (options.compress ? "jquery.validate.min.js" : "jquery.validate.js"), build.assets.js + "vendor/jquery-validation", "validate.js");

    // eonasdan-bootstrap-datetimepicker http://eonasdan.github.io/bootstrap-datetimepicker/Installing/
    copyNodeModule("eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js", build.assets.js + "vendor/bootstrap-datetimepicker");

    // doT.js http://olado.github.io/doT/index.html
    copyNodeModule("dot/" + (options.compress ? "doT.min.js" : "doT.js"), build.assets.js + "vendor/dot", "doT.js");

    // Object.assign polyfill
    copyNodeModule("es6-object-assign/dist/" + (options.compress ? "object-assign-auto.min.js" : "object-assign-auto.js"), build.assets.js + "vendor/rubennorte", "object-assign-auto.js")

    gulp.src(options.paths.nodeModules + 'es6-promise-shim/dist/es6-promise-shim.min.js')
      .pipe(plugins.rename('es6-promise-shim.js'))
      .pipe(gulp.dest(options.paths.build.assets.js + "vendor/rousan"));

    gulp.src(options.paths.nodeModules + 'whatwg-fetch/dist/fetch.umd.js')
      .pipe(plugins.rename('fetch.js'))
      .pipe(gulp.dest(options.paths.build.assets.js + 'vendor/whatwg-fetch'));

    // Copy js files
    copy(dev.root + "assets/js/*.js", build.assets.js);
    copy(dev.root + "assets/js/components/*.js", build.assets.js + "components");
    copy(dev.root + "assets/js/utilities/*.js", build.assets.js + "utilities");
    copy(dev.root + "assets/js/polyfill/*.js", build.assets.js + "polyfill");

      //TABS
    copyNodeModule("@components/jquery-accessible-tabpanel-aria/jquery-accessible-tabs.js", build.assets.js + "vendor/jquery-accessible-tabpanel-aria");

    // Mock Data
    gulp.src(options.paths.dev.assets.mock)
      .pipe(gulp.dest(options.paths.build.assets.mock));

    // annyang voice recognititon
    copyNodeModule("annyang/" + (options.compress ? "annyang.min.js" : "annyang.js"), build.assets.js + "vendor/annyang", "annyang.js");
  }
};
