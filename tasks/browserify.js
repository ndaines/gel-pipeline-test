var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');

module.exports = function(gulp, plugins, options, errorHandler) {  
  function bundleJS(entryPoint, outputName, outputDestination) {
    const b = browserify({
      entries: entryPoint,
      debug: true,
    })
    .transform(babelify.configure({
      presets : ["@babel/preset-env"]
    }))

    return b.bundle()
      .pipe(source(outputName))
      .pipe(buffer())
      .pipe(gulp.dest(outputDestination))
  }

  return function () {
    bundleJS('./src/assets/js/components/socialMedia/main.js', 'socialMedia.js', './dist/assets/js/components');
  }
}