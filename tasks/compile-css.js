"use strict"

module.exports = function(gulp, plugins, options, errorHandler, argv, browserSync) {
  function copy(source, destination) {
    return gulp.src(source).pipe(gulp.dest(destination))
  }

  function copyNodeModule(source, destination) {
    return copy(options.paths.nodeModules + source, destination);
  }

  function stream(gulpTask) {
    gulpTask.pipe(browserSync.stream());
  }

  return function() {
    const { build } = options.paths;

    stream(copyNodeModule("bootstrap/dist/css/bootstrap.min.css", build.assets.css + "vendor/bootstrap"));
    stream(copyNodeModule("@fortawesome/fontawesome-free/css/all.css", build.assets.css + "vendor/fontawesome"));
    stream(copyNodeModule("@fortawesome/fontawesome-pro/css/all.css", build.assets.css + "vendor/fontawesome-pro"));
    stream(copyNodeModule("eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker-standalone.css", build.assets.css + "vendor/bootstrap-datetimepicker"));
    stream(copyNodeModule("eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css", build.assets.css + "vendor/bootstrap-datetimepicker"));

    return gulp.src(options.paths.dev.assets.sass)     // Convert SASS to CSS
      // Stop gulp from crashing on error
      .pipe(plugins.plumber({
        errorHandler: errorHandler
      }))

      .pipe(plugins.sass({
        outputStyle: options.compress ? "compressed" : "expanded"
      }))

      // Append any missing vendor prefixes for CSS3 properties
      .pipe(plugins.autoprefixer({
          browsers: ["last 2 versions", "IE 9", "IE 10"],
          cascade: false
      }))


      // If the --compress flag is true then compress the CSS
      // don't run autoprefixer because it strips out important ie9 stuff
      .pipe(plugins.if(options.compress, plugins.cssnano({autoprefixer: false})))

      // Write the CSS files to disk
      .pipe(gulp.dest(options.paths.build.assets.css))

      // Inject CSS
      .pipe(browserSync.stream())
  }
};
