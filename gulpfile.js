"use strict"

// Load required libraries
var gulp            = require("gulp"),
    runSequence     = require("run-sequence"),
    argv            = require("yargs").argv,
    pug             = require("pug"),
    plugins         = require("gulp-load-plugins")(),
    browserSync     = require("browser-sync").create(),
    git             = require("gulp-git"),
    KarmaServer     = require("karma").Server,
    util            = require("util"),
    readytodeploy   = "no",
    templatesBuilt  = "no",
    errorHandler    = function (error) {
      console.log(error.toString())
      this.emit('end');
    }

// // Allow use of console.log in nodejs files
// console.log = function(d) {
//   process.stdout.write(d + "\n")
// }

// Define options and paths
var options = {
  compress:               argv.compress || false,
  port:                   argv.port || 3000,
  paths: {
    bower:                "bower_components/",
    nodeModules:          "node_modules/",
    dev: {
      root:               "src/",
      index:              "src/*.pug",
      templates:          "src/assets/templates/*.pug",
      components:         "src/assets/templates/components/*.pug",
      assets: {
        root:             "src/assets/",
        sass:             "src/assets/sass/**/*.{scss,sass}",
        js:               "src/assets/js/**/*.{js,es6,es7,es}",
        jsonConfig:       "src/assets/js/config/",
        fonts:            "src/assets/fonts/**/*.{eot,svg,ttf,woff,woff2,otf}",
        images:           "src/assets/images/**/*.{gif,jpg,svg,png}",
        pug:              "src/assets/pug/**/*.pug",
        searchSPAViews:   "src/assets/js/apps/search/views/**/*.pug",
        mock:             "src/assets/mock/*.json"
      }
    },
    build: {
      root:               "dist/",
      templates:          "dist/*.html",
      components:         "dist/components/",
      assets: {
        root:             "dist/assets/",
        css:              "dist/assets/css/",
        js:               "dist/assets/js/",
        fonts:            "dist/assets/fonts/",
        images:           "dist/assets/images/",
        matrixImages:     "dist/assets/css/images/",
        components:       "dist/components/",
        mock:             "dist/mock/"
      }
    }
  },
  deployment: {
    source:               "dist/**/*.*",
    environment:          undefined,
    repository: {
      root:               "../dec-ce.bitbucket.org/",
      deploymentLocation: "../dec-ce.bitbucket.org/gel/"
    },
    args: {
      args:               "",
      cwd:                "../dec-ce.bitbucket.org/",
      quiet:              false
    }
  }
}

// Delete everything in the "build" folder
gulp.task("clean", require("./tasks/clean")(gulp, plugins, options));

// Copy bower libraries
gulp.task("copy", require("./tasks/copy")(gulp, plugins, options, errorHandler));

// Copy fonts found in the projects source files
gulp.task("fonts", require("./tasks/fonts")(gulp, plugins, options, errorHandler));

// Copy images (svg, gif, png, jpg) found in the projects source files
gulp.task("images", require("./tasks/images")(gulp, plugins, options, errorHandler, argv));

// Git repository short hash
gulp.task("revision", require("./tasks/revision")(gulp, plugins, options));

// pug templating engine
const pugTask = require("./tasks/pug")(gulp, plugins, options, errorHandler, pug);
gulp.task("pug:components", pugTask.components);
gulp.task("pug:index", pugTask.index);
gulp.task("pug:templates", pugTask.index);

gulp.task("pug", function(cb) {
  return runSequence("revision", ["pug:components", "pug:templates", "pug:index"], cb);
});

// Compile CSS (SASS)
gulp.task("compile-css", require("./tasks/compile-css")(gulp, plugins, options, errorHandler, argv, browserSync));

// convert ECMAScript 6 & 7 code to ECMAScript 5 code
gulp.task("babel", require("./tasks/babel")(gulp, plugins, options, errorHandler));

// Build Search SPA
gulp.task("search-spa-build", require("./tasks/search-spa-build")(gulp, plugins, options, errorHandler));
gulp.task("search-spa-views", require("./tasks/search-spa-views")(gulp, plugins, options, errorHandler));

// Web server and livereload
gulp.task("browser-sync", require("./tasks/browser-sync")(gulp, browserSync, options, errorHandler));

// Bundle with Browserify
gulp.task("browserify", require('./tasks/browserify')(gulp, plugins, options, errorHandler));

// Karma
gulp.task("karma", require("./tasks/karma")(gulp, plugins, options, errorHandler, KarmaServer));


// Alias for karma
gulp.task("test", ["karma"]);

// Copy Assets for DoE Search
gulp.task("doe-search-copy", require("./tasks/doe-search-copy")(gulp, plugins, options, errorHandler))



// ------------------------ //
//    DEVELOPMENT TASKS     //
// ------------------------ //

// Build Copy - All the required copy tasks in order
gulp.task("build-copy", function(cb) {
  return runSequence("clean", ["copy", "images", "fonts",  "doe-search-copy"], cb)
});

// Build pug - All the required pug tasks in order
gulp.task("build-pug", function (cb) {
  return runSequence('pug', cb);
});

gulp.task("set-ready", function() {
  return readytodeploy="yes"
})

gulp.task("set-templates-built", function() {
  return templatesBuilt="yes"
});

// Build CSS - All the required pug tasks in order
gulp.task("build-css", function() {
  return gulp.start("compile-css")
});

// Build JS - All the required JS tasks in order
gulp.task("build-js", function(cb) {

  return runSequence("babel", "browserify", cb)
});

// Build Task - Compiles all template and code from the src into the dist folder
gulp.task("build", function(cb) {
  return runSequence("build-copy", "build-pug", ["build-css", "build-js"],"set-ready", cb)
});

// Start task - runs Browser Sync so things can be served
gulp.task("start", ["build"], function() {
  return gulp.start("browser-sync")
});

// Watch task. Uses BrowserSync to provide a local development web server with livereload capability e.g.
//
// $ gulp watch
//
// You can optionally pass the "compress" CLI parameter to serve compressed assets e.g.
//
// $ gulp watch --compress
//

gulp.task("watch", ["start"], function() {

  // Process tasks
  gulp.watch(options.paths.dev.assets.sass, ["compile-css"]);
  gulp.watch(options.paths.dev.assets.js, ["babel"]);
  gulp.watch('src/assets/js/**/**/*.{js,es6,es7,es}', ['browserify']);
  gulp.watch(options.paths.dev.assets.fonts, ["fonts"]);
  gulp.watch(options.paths.dev.assets.images, ["images"]);
  gulp.watch([options.paths.dev.index, options.paths.dev.components, options.paths.dev.templates, options.paths.dev.assets.pug], ["pug"]);

  // Watch for JS and HTML
  gulp.watch([options.paths.build.assets.js + "**/*.js"], browserSync.reload);


  gulp.watch('dist/*.html').on('change', browserSync.reload);
  gulp.watch('dist/components/*.html').on('change', browserSync.reload);
});

// ------------------------ //
//    DEPLOYMENT TASKS      //
// ------------------------ //


// Pull first to catch any updates
gulp.task("git-pull",  function(cb){
  return git.pull('origin', 'master', { cwd: options.deployment.repository.root, maxBuffer: Infinity }, function (err) {
      if (err) {
        throw err
      } else {
        runSequence('git-deploy', cb)
      }
  });
});

// Pull first to catch any updates
gulp.task("git-pull-manual",  function(cb){
  return git.pull('origin', 'master', { cwd: options.deployment.repository.root, maxBuffer: Infinity }, function (err) {
      if (err) {
        throw err
      } else {
        runSequence('git-deploy-manual', cb)
      }
  });
});

// Copy the files
gulp.task("git-cp", function() {

  return gulp.src(options.deployment.source)
    .pipe(gulp.dest(options.deployment.repository.deploymentLocation))
    .on('error', function(err){
      console.log("Error", err)
    })
    .on("end", function(){
      console.log("completed file copy")
    })
})


// Copy the files
gulp.task("git-cp2", function() {
  if (readytodeploy == "yes") {
  console.log("starting file copy")
  return gulp.src("dist/**/*")
    .pipe(gulp.dest(options.deployment.repository.deploymentLocation))
    .on('error', function(err){
      console.log("Error", err)
    })
    .on("end", function(){
      console.log("completed file copy to ", options.deployment.repository.deploymentLocation)
    })
  } else {
    setTimeout(function(){ runSequence("git-cp2")}, 500)
  }
})

// Copy the files
gulp.task("git-cp-manual", function() {
  console.log("starting file copy")
  return gulp.src("dist/**/*")
    .pipe(gulp.dest(options.deployment.repository.deploymentLocation))
    .on('error', function(err){
      console.log("Error", err)
    })
    .on("end", function(){
      console.log("completed file copy to ", options.deployment.repository.deploymentLocation)
    })
})


// Add any new files to the repo
gulp.task("git-add", ["git-cp2"], function() {
  return gulp.src("./.", {cwd: options.deployment.repository.root})
    .pipe(git.add({cwd: options.deployment.repository.root}, function(err) {
      if (err) {
        throw err
      } else {
        console.log('Added new files!');
      }
    }))
});
// Add any new files to the repo
gulp.task("git-add-manual", ["git-cp-manual"], function() {
  return gulp.src("./.", {cwd: options.deployment.repository.root})
    .pipe(git.add({cwd: options.deployment.repository.root}, function(err) {
      if (err) {
        throw err
      } else {
        console.log('Added new files!');
      }
    }))
});

// Automated commit message
gulp.task("git-commit", ["git-add"], function() {
  return gulp.src("./", {cwd: options.deployment.repository.root})
    .pipe(git.commit("Automated deployment to gel " + options.deployment.environment + " repository", {cwd: options.deployment.repository.root}, function(err) {
        this.emit("end")
        console.log("The " + options.deployment.environment + " environment is already up to date. Nothing to be added :D")
    })
  )
})

gulp.task("git-commit-manual", ["git-add-manual"], function() {
  return gulp.src("./", {cwd: options.deployment.repository.root})
    .pipe(git.commit("Automated deployment to gel " + options.deployment.environment + " repository", {cwd: options.deployment.repository.root}, function(err) {
        this.emit("end")
        console.log("The " + options.deployment.environment + " environment is already up to date. Nothing to be added :D")
    })
  )
})

// Push that code.
gulp.task("git-deploy", ["git-commit"], function(cb) {
    return git.push("origin", "master", options.deployment.args, function(err) {
      if (err) {
        throw err
      } else {
        console.log("You've successfully updated the gel " + options.deployment.environment + " environment!")
        gulp.src(__filename)
        .pipe(plugins.open({uri: "https://dec-ce.bitbucket.io/gel/" + options.deployment.environment + "/"}))
      }
    })
})

gulp.task("git-deploy-manual", ["git-commit-manual"], function(cb) {
    return git.push("origin", "master", options.deployment.args, function(err) {
      if (err) {
        throw err
      } else {
        console.log("You've successfully updated the gel " + options.deployment.environment + " environment!")
        gulp.src(__filename)
        .pipe(plugins.open({uri: "https://dec-ce.bitbucket.io/gel/" + options.deployment.environment + "/"}))
      }
    })
})


// Automated Deployment
// To deploy to dec-ce.bitbucket.org/gel/(string) run the "deploy" task and pass a string of your choice (default: undefined)
//
// $ gulp deploy --env=review
//
//
gulp.task("deploy", function(cb) {

  options.compress = true

  // Set the environment if CLI parameter is set
  options.deployment.environment = argv.env !== undefined ? argv.env : options.deployment.environment

  if (options.deployment.environment !== undefined) {
    // Assign the new deployment location
    options.deployment.repository.deploymentLocation += options.deployment.environment
    // Run the Deployment
    runSequence("build",  "git-pull", cb)
  } else {
    console.log("Please supply a deployment environment. e.g. $ gulp deploy --env=GXF-420")
  }
})

gulp.task("deploy-s3", function(cb) {

  options.compress = true

  // Set the environment if CLI parameter is set
  options.deployment.environment = argv.env !== undefined ? argv.env : options.deployment.environment

  if (options.deployment.environment !== undefined) {
    // Assign the new deployment location
    options.deployment.repository.deploymentLocation += options.deployment.environment
    // Run the Deployment
    runSequence("build", cb)
  } else {
    console.log("Please supply a deployment environment. e.g. $ gulp deploy --env=GXF-420")
  }
})



gulp.task("message:deploy-manual-copy", function () {
  console.log("Now do deploy-manual-copy --env=X")
});
gulp.task("deploy-manual-build", function(cb){
  options.compress=true;
  runSequence('build', "message:deploy-manual-copy", cb);
})

gulp.task("deploy-manual-copy", function(db) {
  options.deployment.environment = argv.env !== undefined ? argv.env : options.deployment.environment

  if (options.deployment.environment !== undefined) {
    // Assign the new deployment location
    options.deployment.repository.deploymentLocation += options.deployment.environment
    // Run the Deployment
    gulp.start("git-pull-manual")
  } else {
    console.log("Please supply a deployment environment. e.g. $ gulp deploy --env=GXF-420")
  }

})

// Deploy for regression tests
gulp.task("deploy-test", function(cb) {
  argv.env = "test"
  gulp.start("deploy")
})

// Deploy for master staging
gulp.task("deploy-master", function(cb) {
  argv.env = "master"
  gulp.start("deploy")
})







// ------------------------ //
//    AUTOMATED RELEASE     //
// ------------------------ //

// Automated master merge and tagging.
// This task merges develop into master, tags the commit, updates the master staging site
//
// $ gulp master-merge --tag=X.X.X
gulp.task("master-merge", function(cb) {
  options.compress = true
  runSequence(
    "master-merge_check-branch",
    "master-merge_check-commit",
    "build",
    "test",
    "master-merge_checkout-master",
    "master-merge_merge-develop",
    "master-merge_check-tag",
    cb
  )
})

// Check that we're in the development branch
gulp.task("master-merge_check-branch", function(cb) {

  console.log("gel: checking that we're in the develop branch")
  plugins.git.exec({args: "rev-parse --abbrev-ref HEAD"}, function(err, output) {
    if (err) { throw err } else {
      console.log("gel: current branch: " + output)
      if (output.replace(/\n/g, "") === "develop") {
        console.log("gel: nice, we're in the correct branch")
        cb()
      } else {
        console.log("!!! gel ERROR: You need to be in the develop branch!")
      }
    }
  })
})

// Check that the branch is up to date
gulp.task("master-merge_check-commit", function(cb) {

  console.log("gel: checking that develop branch is up to date")
  plugins.git.status({args: "--porcelain"}, function(err, output) {
   if (err) { throw err } else {
      if (output.length === 0) {
        console.log("gel: great, branch is up to date")
        cb()
      } else {
        console.log("!!!! gel ERROR: You have to commit or pull changes on the develop branch")
      }
    }
  })
})


// Checkout the master branch
gulp.task("master-merge_checkout-master", function(cb) {

  console.log("gel: checking out master branch")

  plugins.git.exec({args: "checkout master"}, function(err, output) {
    if (err) { throw err } else {
      console.log("gel: pulling the latest from master")

      plugins.git.exec({args: "pull"}, function(err, output) {
         if (err) { throw err } else {
          cb()
        }
      })
    }
  })
})

// Merge Develop into Master
gulp.task("master-merge_merge-develop", function(cb) {

  console.log("gel: merging develop branch into master branch")
  plugins.git.exec({args: "merge develop"}, function(err, output) {
    if (err) {
      console.log("!!!! gel ERROR: You have merge conflicts! You'll need to fix those conflicts, commit the changes, and do the following steps manually:")
      console.log("-- $ git push -u origin master --tags")
      console.log("-- $ gulp deploy-master")
    } else {
      console.log("gel: merge successful! Tagging release...")
      cb()
    }
  })
})

// Check for the new tags
gulp.task("master-merge_check-tag", function(cb) {

  console.log("gel: checking that the tag is new")
  console.log("gel: you applied this tag: " + argv.tag)
  if (argv.tag) {
    plugins.git.exec({args:"tag " + argv.tag}, function(err, output) {
      if (err) {
        console.log("!!!! gel ERROR: make sure the tag is new (" + argv.tag + ")")
      } else {
        console.log("gel: The tag is new. You've been cleared to continue...")

        plugins.git.exec({args: "push -u origin master --tags"}, function(err, output) {
          if (err) { throw err } else {
            console.log("gel: Changes pushed. Release made! Give yourself a pat on the back!!")
            console.log("gel: Now please deploy to our master staging $ gulp deploy-master")
          }
        })
      }
    })
  } else {
    console.log("!!!! gel ERROR: You didn't apply a tag e.g: $ gulp master-merge --tag=X.X.X")
  }
})




// Default run task
gulp.task("default", function(cb) {
  gulp.start("watch", cb)
})
