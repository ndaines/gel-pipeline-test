// Karma configuration
// Generated on Sun Nov 08 2015 18:36:31 GMT+1100 (AEDT)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: [ 'jasmine-jquery', 'jasmine', 'requirejs' ],


    // list of files / patterns to load in the browser
    files: [
      'test/test-main.js',
      { pattern: 'test/spec/**/*Spec.js', included: false },
      { pattern: 'test/fixtures/**/*.html', included: false },
      { pattern: 'dist/assets/**/*', included: false },
      { pattern: 'dist/assets/js/**/*.js', included: false }
    ],


    // list of files to exclude
    exclude: [
      'dist/assets/js/main.js'
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,

    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['chrome_custom', 'firefox_custom'],

    customLaunchers: {

      chrome_custom: {
        base: 'Chrome',
        flags: ['--window-size=1024,786']
      },

      firefox_custom: {
        base: 'Firefox',
        flags: ['width 1024', 'height 786']
      }
    },

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true,

    // Concurrency level
    // how many browser should be started simultanous
    concurrency: Infinity,

    // Which plugins to enable
    plugins: [
      'karma-requirejs',
      'karma-chrome-launcher',
      'karma-firefox-launcher',
      'karma-jasmine-jquery-2',
      'karma-jasmine'


    ],

  })
}
